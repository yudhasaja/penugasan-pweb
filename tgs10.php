<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 10</title>
    <style>
        body {
            background-color: #f0f0f0;
            font-family: Arial, sans-serif;
        }

        .container {
            max-width: 400px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        h1 {
            text-align: center;
            color: #333;
            font-size: 24px;
            margin-top: 0;
        }

        label {
            display: block;
            margin-bottom: 10px;
            color: #666;
            font-size: 16px;
        }

        input[type="text"] {
            width: 100%;
            padding: 8px;
            font-size: 16px;
            border-radius: 4px;
            border: 1px solid #ccc;
        }

        button {
            display: block;
            width: 100%;
            padding: 10px;
            font-size: 16px;
            font-weight: bold;
            text-align: center;
            color: #fff;
            background-color: #333;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #555;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            background-color: #f9f9f9;
            border-radius: 4px;
            border: 1px solid #ccc;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Penilaian</h1>

        <form>
            <div>
                <label>Nilai : </label>
                <input name="nilai" type="text" placeholder="Masukkan nilai anda">
            </div>

            <div>
                <button>Submit</button>
            </div>
        </form>

        <div class="result">
            <?php
                $nilai = @$_GET['nilai'];

                if($nilai >= 80 && $nilai <= 100){
                    echo "Nilai Anda A, Baik Sekali";
                }elseif($nilai >= 76.25 && $nilai <= 79.99){
                    echo "Nilai Anda A-, Baik Sekali";
                }elseif($nilai >= 68.75 && $nilai <= 76.24){
                    echo "Nilai Anda B+, Baik";
                }elseif($nilai >= 65.00 && $nilai <=  68.74){
                    echo "Nilai Anda B, Baik";
                }elseif($nilai >= 62.50 && $nilai <= 64.99){
                    echo "Nilai Anda B-, Baik";
                }elseif($nilai >= 57.50 && $nilai <=  62.49){
                    echo "Nilai Anda C+, Cukup";
                }elseif($nilai >= 55.00 && $nilai <=  57.49){
                    echo "Nilai Anda C, Cukup";
                }elseif($nilai >= 43.75 && $nilai <=  51.24){
                    echo "Nilai Anda D+, Kurang";
                }elseif($nilai >=40.00 && $nilai <=  43.74){
                    echo "Nilai Anda D, Kurang";
                }elseif($nilai >= 0.00  && $nilai <= 39.99){
                    echo "Nilai Anda E, Gagal";
                }else{
                    echo "Nilai yang anda masukkan salah";
                }
                
                ?>
                </body>
                </html>
